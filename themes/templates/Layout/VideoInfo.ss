<body class="$ClassName<% if not $Menu(2) %> no-sidebar<% end_if %>" <% if $i18nScriptDirection %>dir="$i18nScriptDirection"<% end_if %>>
<% include Header %>

<div class="main" role="main">
  <% if $CurrentMember %>

    <ul class="breadcrumbs">
      <li><a href="$VideoStage.VideoPhase.HomeURL">$VideoStage.VideoPhase.Surgery.BreadCrumb</a></li>
      <li><a href="$VideoStage.VideoPhase.Link">$VideoStage.VideoPhase.Name</a></li>
      <li><a href="$VideoStage.Link">$VideoStage.Name</a></li>
      <li class="disabled">$title</li>
    </ul>

    <div class="main-heading trail">
      <% if $PrevVideoLink %>
	      <a href="$PrevVideoLink" title="Previous Video - $PrevVideoTitle" class="previous icon"></a>
      <% else %>
        <span class="previous icon disabled"></span>
      <% end_if %>
      <h2>$title</h2>
      <% if $NextVideoLink %>
        <a href="$NextVideoLink" title="Next Video - $NextVideoTitle" class="next icon"></a>
      <% else %>
        <span class="next icon disabled"></span>
      <% end_if %>
    </div>

    <% if $VideoFile %>
      <div class="video_file">
        <video controls poster="/unisports/$PreviewImage.Filename">
          <source src="$VideoFile.URL" type="video/mp4">
            Your browser does not support the video tag.
        </video>
      </div>
    <% end_if %>
    <div class="video-text">
      <span>$RightVideoInfo</span>
      <span>$LeftVideoInfo</span>
      $Description
    </div>

    <div class="add-note">
    <a href="$VideoNotesLink" class="button">Add Note</a>
    </div>
    <% if $NextVideoLink %>
      <div class="next_exercise">
        <p>Next exercise</p>
        <a href="$NextVideoLink" title="Next Video - $NextVideoTitle">$NextVideoTitle</a>
      </div>
    <% else_if $VideoStage.NextStageLink %>
      <div class="next_exercise">
        <p>You have completed this Stage, continue to next Stage</p>
        <a href="$VideoStage.NextStageLink" title="Next Stage - $VideoStage.NextStageTitle">$VideoStage.NextStageTitle</a>
      </div>
    <% else_if $VideoStage.VideoPhase.NextPhaseLink %>
      <div class="next_exercise">
        <p>You have completed this Phase, continue to next Phase</p>
        <a href="$VideoStage.VideoPhase.NextPhaseLink" title="Next Phase - $VideoStage.VideoPhase.NextPhaseName">$VideoStage.VideoPhase.NextPhaseName</a>
      </div>
    <% else %>
	    <div class="next_exercise">
	      <p>Congratulations, you have finished the Program!</p>
	    </div>
    <% end_if %>

  <% else %>
    <p>You must be logged in to view this content</p>
      $Loginform
  <% end_if %>

</div>


<% include VideoFooter %>