<% include VideoHeader %>

<body class="$ClassName<% if not $Menu(2) %> no-sidebar<% end_if %>" <% if $i18nScriptDirection %>dir="$i18nScriptDirection"<% end_if %>>

<% include Header %>
<div class="main" role="main">

  <div class="main-heading">
		<h1>$Title</h1>
	</div>
	<article>

	<% if $CurrentMember %>
		<% with $SurgeryVideos %>
			<h2>ACL pre-surgery</h2>
			<% loop $VideoPhases %>
			<% if $Pos == 3 %>
			<h2>ACL post-surgery</h2>
			<% end_if %>
			<ul class="phase_list">
				<li><a href="$Link">$Name</a></li>
					<% loop $VideoStages %>
					<li><a href="$Link">$Name</a></li>
					<% end_loop %>
			</ul>
			<% end_loop %>
		<% end_with %>
	<% else %>
    <p>You must be logged in to view this content</p>
    $Loginform
  <% end_if %>
	</article>
</div>
<% include VideoFooter %>
