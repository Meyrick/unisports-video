<?php
class UnisportsMemberExtension extends DataExtension {

	static $has_one = array(
		'LastViewedVideo' => 'Video'
	);

	static $has_many = array(
		'RecentlyWatchedVideos' => 'RecentlyWatched',
		"MemberNotes" => "MemberNote"
	);
}