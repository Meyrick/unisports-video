<?php

class SurgeryHomePage extends VideoModulePage
{

	static $has_one = array(
		'Surgery' => 'Surgery'
	);

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->removeByName('Content');
		$surgeryList = Surgery::get()->map('ID', 'SurgeryName')->toArray();
  		$surgerySelect = DropdownField::create('SurgeryID', 'Surgery')->setSource($surgeryList)->setEmptyString('Please select a Surgery');
 		$fields->addFieldToTab('Root.Main', $surgerySelect);
		return $fields;
	}
}

class SurgeryHomePage_Controller extends VideoModulePage_Controller
{

	static $allowed_actions = array(
		'show'
	);

	private static $url_handlers = array(
		'show/$Phase/$Stage/$Video' => 'show'
	);

	public function SurgeryVideos(){
		return $this->Surgery();
	}

	/**
	 * @param SS_HTTPRequest $request
	 * @return HTMLText
	 */
	public function show(SS_HTTPRequest $request) {
		$phase = null;
		if($request->param('Phase')){
			$phase = VideoPhase::get()->filter(array('URLSegment' => Convert::raw2sql($request->param('Phase'))));
		}else{
			return $this->httpError('404');
		}

		if($phase->exists() && $request->param('Phase') && !$request->param('Stage') && !$request->param('Video')){
			return $this->displayPhase($phase->First());
		} else if($phase->exists() && $request->param('Phase') && $request->param('Stage') && !$request->param('Video')) {
			return $this->showVideos($phase->First()->VideoStages()->filter(array('URLSegment' => $request->param('Stage'))));
		} else if($phase->exists() && $request->param('Phase') && $request->param('Stage') && $request->param('Video')){
			$stage = $phase->First()->VideoStages()->filter(array('URLSegment' => $request->param('Stage')));
			if($stage->exists()){
				$video = $stage->First()->Videos()->filter(array('URLSegment' => $request->param('Video')));
				if($video->exists()){
					return $this->displayvideo($video);
				}
				return $this->httpError('404');
			}
		}else{
			return $this->httpError('404');
		}
	}


	/**
	 * @param $phase
	 * @return HTMLText|void
	 */
	public function displayPhase($phase){
		if($phase){
			if($phase){
				return $this->customise($phase)->renderWith(array('PhaseInfo', 'VideoPhaseSubPages'));
			}
			return $this->httpError(404, 'Not found');
		}
	}


	/**
	 * @param $stage
	 * return a hasmanylist of videos to display on stage landing page
	 * @return mixed
	 */
	public function showVideos($stage){
		return $this->customise(array('StageVideos' => $stage->First()->Videos(), 'Stage' => $stage->First()))
			->renderWith(array('StageLanding', 'VideoPhaseSubPages'));
	}

	/**
	 * @return HTMLText|void
	 */
	public function displayvideo($video){
		//set the members last viewed video to the one we are displaying
		if($video->exists()){
			$video = $video->First();
			if(Member::currentUser() && $video){
					$member = Member::currentUser();
					$member->LastViewedVideoID = $video->ID;
					$this->addToRecentlyWatched($member, $video);
				}
			return $this->customise($video)->renderWith(array('VideoInfo', 'VideoPhaseSubPages'));
		} else {
			return $this->httpError(404, 'Not found');
		}
	}


	/**
	 * @param $member
	 * @param $video
	 */
	private function addToRecentlyWatched($member, $video){
		$recentlyWatched = $member->RecentlyWatchedVideos()->filter(array(
			'MemberID' => $member->ID,
			'VideoID' => $video->ID
		));

		//if video already exists in the relationship with current member just updated edited time
		//else add a relationship
		if($recentlyWatched->exists()){
			$member->RecentlyWatchedVideos()->remove($recentlyWatched->first());
		}

		$recentlyWatched = new RecentlyWatched();
		$recentlyWatched->VideoID = $video->ID;
		$recentlyWatched->write();
		$member->RecentlyWatchedVideos()->add($recentlyWatched);
		return;
	}
}


