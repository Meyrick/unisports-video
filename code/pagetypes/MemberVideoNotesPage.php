<?php
class MemberVideoNotesPage extends VideoModulePage
{

}


class MemberVideoNotesPage_Controller extends VideoModulePage_Controller
{
    public static $allowed_actions = array(
        'getnotesform',
        'savenotes'
    );

    public function init()
    {
        parent::init();
        Requirements::javascript('framework/thirdparty/jquery/jquery.min.js');
        Requirements::javascript('framework/thirdparty/jquery-ui/jquery-ui.min.js');
        Requirements::javascript('framework/thirdparty/jquery-ui/datepicker/i18n/jquery.ui.datepicker-en-GB.js');
        Requirements::javascript(VIDEO_MODULE_PATH . '/code/javascript/VideoNotes.js');
        Requirements::css('framework/thirdparty/jquery-ui-themes/smoothness/jquery-ui.css');
    }

    public function MemberNotes()
    {
        $member = Member::currentUser();
        return $member->MemberNotes();
    }

    public function getnotesform(SS_HTTPRequest $request)
    {
        if($request->isAjax()) {
            $data = $request->getVars();
            if (!isset($data['id'])) {
                return $this->renderWith('NotesEditForm');
            } else {
                $note = MemberNote::get_by_id('MemberNote', Convert::raw2sql($data['id']));
                if($note) {
                    return $this->renderWith('NotesEditForm', $note);
                }
            }
        }else{
            $this->httpError('404', 'Method not found');
        }
    }


    public function savenotes(SS_HTTPRequest $request)
    {
        if($request->isAjax()) {
            $data = $request->getVars();
            if (!isset($data['id'])) {
                $note = new MemberNote();
                $note->Note = isset($data['note']) ? Convert::raw2sql($data['note']) : null;
                $note->NoteDate = isset($data['notedate']) ? Convert::raw2sql($data['notedate']) : null;
                $note->write();
                Member::currentUser()->MemberNotes()->add($note);
                return $this->renderWith('NotesList', Member::currentUser()->MemberNotes()->sort('LastEdited ASC'));
            } else {
                $note = MemberNote::get_by_id('MemberNote', Convert::raw2sql($data['id']));
                if($note) {
                    $note->Note = isset($data['note']) ? Convert::raw2sql($data['note']) : null;
                    $note->NoteDate = isset($data['notedate']) ? Convert::raw2sql($data['notedate']) : null;
                    $success = $note->write();
                    if ($success) {
                        return $this->renderWith('NotesList', Member::currentUser()->MemberNotes()->sort('LastEdited ASC'));
                    } else {
                        $this->httpError('500', 'Something went wrong');
                    }
                } else {
                    $this->httpError('500', 'Something went wrong');
                }
            }
        }else{
            $this->httpError('404', 'Method not found');
        }
    }
}