<?php
class VideoModulePage extends Page{

}

class VideoModulePage_Controller extends Page_Controller{

    public function init(){
        parent::init();
    }

    public function VideoModulePath()
    {
        return VIDEO_MODULE_PATH;
    }
    
    public function VideoACLHome()
    {
        return SurgeryHomePage::get()->exists() ? SurgeryHomePage::get()->first()->Link() : null;
    }

    public function VideoNotesLink()
    {
        return MemberVideoNotesPage::get()->exists() ? MemberVideoNotesPage::get()->first()->Link() : null;

    }
    
    public function HomePageLink()
    {
        return Director::baseURL();
    }
}