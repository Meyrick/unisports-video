<?php

class MemberLandingPage extends VideoModulePage
{

}

class MemberLandingPage_Controller extends VideoModulePage_Controller
{
	public function init(){
		parent::init();
	}

	public function MemberStep(){
		$member = Member::currentUser();
		if($member && $member->LastViewedVideoID){
			return Video::get()->byID($member->LastViewedVideoID);
		}else{
			//#TODO return a default video
		}
	}

	public function MemberRecentlyWatchedVideos(){
		$recentlyWatched = Member::currentUser()
					->RecentlyWatchedVideos()
					->sort('LastEdited')
					->limit(4);
		$al = new ArrayList();
		foreach($recentlyWatched as $rw){
			$videoObject = Video::get()->byID($rw->VideoID);
			$al->push($videoObject);
		}
		return $al;
	}
}