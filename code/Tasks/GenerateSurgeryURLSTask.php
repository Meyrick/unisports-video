<?php
class GenerateSurgeryURLSTask extends BuildTask{

	protected $description = 'Genereates URL Segments for all existing Phase/Stage/Video DataObjects';

	public function run($request){
		$phases = VideoPhase::get()->where('Name IS NOT NULL');
		$stages = VideoStage::get()->where('Name IS NOT NULL');
		$videos = Video::get()->where('Title IS NOT NULL');

		foreach($phases as $phase){
			$phase->URLSegment = Surgery::generate_url($phase->Name());
			$phase->write();
			echo "Created $phase->Name as $phase->URLSegment <br />";
		}

		foreach($stages as $stage){
			//Boom
			$stage->URLSegment = Surgery::generate_url($stage->Name());
			$stage->write();
			echo "Created $stage->Name as $stage->URLSegment <br />";
		}

		foreach($videos as $video){
			//Boom
			$video->URLSegment = Surgery::generate_url($video->Title);
			$video->write();
			echo "Created $video->Title as $video->URLSegment <br />";
		}

		echo "<h2>BOOM! All done. Go find Videos by more meaningful URLs</h2>";
	}
}