jQuery.noConflict();

(function($) {

    var activeEdit = false;

    //display the edit notes form when a user clicks on 'add note'
    $(".add-note").click(function(e){
        var url = [location.protocol, '//', location.host, location.pathname].join('');
        if(!activeEdit) {
            $.ajax({
                url: url + "/getnotesform"
            }).done(function (data) {
                $(".edit-form").html(data);
                $("#datepicker").datepicker();
                activeEdit = true;
            }).error(function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            });
        }
    });

    //save the notes data
    $(".edit-form").on("click", ".save-note", function(){
        var errorField = $(".note-error"),
            memberNote = $(".member-note-input").val(),
            noteDate = $(".note-date").val(),
            url = [location.protocol, '//', location.host, location.pathname].join('');

        if(!memberNote || !noteDate){
            errorField.text('Both fields are required');
        } else {
            errorField.text();
            $.ajax({
                url: url + "/savenotes",
                data: {note: memberNote, notedate: noteDate }
            }).done(function(data) {
                $(".member-notes-list").html(data);
                $(".edit-form").html('');
                activeEdit = false;
            }).error(function( jqXHR, textStatus, errorThrown ) {
                console.log(textStatus, errorThrown);
            });
        }
    });

    //get inline edit form
    $(".member-notes-list").on("click", ".member-note", function(){
        var id = $(this).data().id,
            self = $(this),
            url = [location.protocol, '//', location.host, location.pathname].join('');

        if(id && !activeEdit){
            $.ajax({
                url: url + "/getnotesform",
                data: {id: id}
            }).done(function(data) {
                activeEdit = true;
                self.html(data);
                $( "#datepicker" ).datepicker();
            }).error(function( jqXHR, textStatus, errorThrown ) {
                console.log(textStatus, errorThrown);
            });
        }
    });


    //save the inline editable values
    $(".member-notes-list").on("click", ".save-note", function(){
        var errorField = $(this).parent().find(".note-error"),
            memberNote = $(this).parent().find(".member-note-input").val(),
            noteDate = $(this).parent().find(".note-date").val(),
            id = $(this).parent().find(".note-id").val(),
            url = [location.protocol, '//', location.host, location.pathname].join('');

        console.log(memberNote, noteDate);
        if(!memberNote || !noteDate){
            errorField.text('Both fields are required');
        } else {
            errorField.text();
            $.ajax({
                url: url + "/savenotes",
                data: {note: memberNote, notedate: noteDate, id: id}
            }).done(function(data) {
                $(".member-notes-list").html(data);
                $(".edit-form").html('');
                activeEdit = false;
            }).error(function( jqXHR, textStatus, errorThrown ) {
                console.log(textStatus, errorThrown);
            });
        }
    });


}(jQuery));