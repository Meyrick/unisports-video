<?php
class MemberNotesAdmin extends ModelAdmin{
    private static $managed_models = array(
        'MemberNote'
    );

    private static $url_segment = 'membernotes';

    private static $menu_title = 'Member Notes Admin';
}