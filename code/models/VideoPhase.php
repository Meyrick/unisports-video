<?php

class VideoPhase extends DataObject{

	static $db = array(
		"Name" => "Varchar(255)",
		"PhasePeriod" => "Varchar(255)",
		"PhaseDescription" => "HTMLText",
		"PhaseGoalsText" => "HTMLText",
		"SortOrder" => "Int",
		"URLSegment" => "Varchar(255)"
	);

	static $has_many = array(
		"VideoStages" => "VideoStage"
	);

	static $has_one = array(
		"PhaseImage" => "Image",
		"PhaseVideo" => "File",
		"Surgery" => "Surgery"
	);

	static $summary_fields = array(
		"Name",
		"PhasePeriod"
	);

	static $default_sort='SortOrder';

	public function getCMSFields(){
		$fields = parent::getCMSFields();

		$fields->removeByName('SortOrder');
		$fields->removeByName('VideoStages');

		$config = $config = GridFieldConfig_RecordEditor::create();
		$gf = new GridField('VideoStages', "Edit Video Stages", $this->VideoStages());
		$gf->setConfig($config);
		if ($this->ID) {
			$config->addComponent(new GridFieldOrderableRows('SortOrder'));
		}
		$fields->addFieldToTab('Root.VideoStages', $gf);

		if($this->ID){
			$fields->addFieldToTab('Root.Main', new LiteralField('LinkLabel', 'The Link to this phase is.'));
			$fields->addFieldToTab('Root.Main', new LiteralField('Link', '<a href="' . $this->Link() . '">' . $this->Link() . '</a>'));
		}
		return $fields;
	}

	public function Link(){
		$home = SurgeryHomePage::get();
		if($home->exists()) {
			return $home->filter(array('SurgeryID' => $this->Surgery()->ID))->First()->Link('show/' . $this->URLSegment);
		}
	}

	public function NextPhase(){
		return VideoPhase::get()->sort('SortOrder ASC')->filter(array('SortOrder:GreaterThan' => $this->SortOrder));
	}

	public function NextPhaseLink(){
		$nextPhase = $this->NextPhase();
		if($nextPhase && $nextPhase->exists()){
			return $nextPhase->First()->Link();
		}else{
			return null;
		}
	}

	public function NextPhaseName(){
		$nextPhase = $this->NextPhase();
		if($nextPhase && $nextPhase->exists()){
			return $nextPhase->First()->Name();
		}else{
			return null;
		}
	}

	public function HomeURL(){
		$home = SurgeryHomePage::get()->filter(array('SurgeryID' => $this->SurgeryID));
		if($home && $home->exists()){
			return $home->First()->Link();
		}else{
			return null;
		}
	}

	//just cos SS returns the ClassName attr instead of Name
	public function Name(){
		return $this->Name;
	}

	protected function onBeforeWrite(){
		parent::onBeforeWrite();
		if($this->URLSegment){
			$this->URLSegment = Surgery::generate_url($this->URLSegment);
		}else{
			$this->URLSegment = Surgery::generate_url($this->Name());
		}
	}
}