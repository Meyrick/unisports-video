<?php

class MemberNote extends DataObject{

    static $db = array(
        "Note" => "Text",
        "NoteDate" => "Date"
    );

    static $has_one = array(
        "Member" => "Member"
    );
    
    function canView($member = false)
    {
        if (!$member) $member = Member::currentUser();
        if (Permission::check('ADMIN') || $member->ID == $this->Member()->ID) {
            return true;
        }
        return false;
    }
    
    function canEdit($member = null)
    {
        return $this->canView();
    }
}