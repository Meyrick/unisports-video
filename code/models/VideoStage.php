<?php

class VideoStage extends DataObject{

	static $db = array(
		"Name" => "Varchar(255)",
		"SortOrder" => "Int",
		"URLSegment" => "Varchar(255)"
	);

	static $has_one = array(
		"VideoPhase" => "VideoPhase"
	);

	static $has_many = array(
		"Videos" => "Video"
	);

	static $default_sort = 'SortOrder';

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->removeByName('SortOrder');
		$fields->removeByName('Videos');

		$config = GridFieldConfig_RecordEditor::create();
		$gf = new GridField('Videos', "Edit Videos", $this->Videos());
		$gf->setConfig($config);
		if ($this->ID) {
			$config->addComponent(new GridFieldOrderableRows('SortOrder'));
		}
		$fields->addFieldToTab('Root.Videos', $gf);

		if($this->ID && $this->ID != 0){
			$fields->addFieldToTab('Root.Main', new LiteralField('LinkLabel', 'The Link to this stage is.'));
			$fields->addFieldToTab('Root.Main', new LiteralField('Link', '<a href="' . $this->Link() . '">' . $this->Link() . '</a>'));
		}
		return $fields;
	}

	public function Link(){
		return Controller::join_links($this->VideoPhase()->Link(), $this->URLSegment);
	}


	public function NextStage(){
		return VideoStage::get()->sort('SortOrder ASC')->filter(array('VideoPhaseID' => $this->VideoPhaseID, 'SortOrder:GreaterThan' => $this->SortOrder));
	}

	public function PrevStage(){
		$prevStage = null;
		$prevStage = VideoStage::get()->sort('SortOrder DESC')->filter(
			array('VideoPhaseID' => $this->VideoPhaseID,  'SortOrder:LessThan' => $this->SortOrder)
		);
		return $prevStage;
	}

	//Computed Fields For Template
	public function NextStageLink(){
		$nextStage = $this->NextStage();
		if($nextStage && $nextStage->exists()){
			return $nextStage->First()->Link();
		}else{
			return null;
		}
	}

	public function NextStageTitle(){
		$nextStage = $this->NextStage();
		if($nextStage && $nextStage->exists()){
			return $nextStage->First()->Name();
		}else{
			return null;
		}
	}

	public function PrevStageLink(){
		$prevStage = $this->PrevStage();
		if ($prevStage && $prevStage->exists()) {
			return $prevStage->First()->Link();
		} else {
			return null;
		}
	}

	public function PrevStageTitle(){
		$prevStage = $this->PrevStage();
		if ($prevStage && $prevStage->exists()) {
			return $prevStage->First()->Name();
		} else {
			return null;
		}
	}


	//just cos SS returns the ClassName attr instead of Name
	public function Name(){
		return $this->Name;
	}

	protected function onBeforeWrite(){
		parent::onBeforeWrite();
		if($this->URLSegment){
			$this->URLSegment = Surgery::generate_url($this->URLSegment);
		}else{
			$this->URLSegment = Surgery::generate_url($this->Name());
		}
	}
}