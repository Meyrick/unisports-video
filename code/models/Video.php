<?php

class Video extends DataObject{

	static $db = array(
		"Title" => "Varchar(255)",
		"LeftVideoInfo" => "Varchar(255)",
		"RightVideoInfo" => "Varchar(255)",
		"Description" => "HTMLText",
		"SortOrder" => "Int",
		"URLSegment" => "Varchar(255)"
	);

	static $has_one = array(
		"VideoStage" => "VideoStage",
		"VideoFile" => "File",
		"PreviewImage" => "Image"
	);

	static $default_sort = 'SortOrder';

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->removeByName('SortOrder');

		if($this->ID){
			$fields->addFieldToTab('Root.Main', new LiteralField('LinkLabel', 'The Link to this video is.'));
			$fields->addFieldToTab('Root.Main', new LiteralField('Link', '<a href="' . $this->Link() . '">' . $this->Link() . '</a>'));
		}
		return $fields;
	}

	public function Link(){
		return Controller::join_links($this->VideoStage()->Link(), $this->URLSegment);
	}

	public function NextVideo(){
		return Video::get()->sort('SortOrder ASC')->filter(array('VideoStageID' => $this->VideoStageID, 'SortOrder:GreaterThan' => $this->SortOrder));
	}

	public function PrevVideo(){
		$prevVideo = null;
		$prevVideo = Video::get()->sort('SortOrder DESC')->filter(
			array('VideoStageID' => $this->VideoStageID,  'SortOrder:LessThan' => $this->SortOrder)
		);
		return $prevVideo;
	}

	public function NextVideoLink(){
		$nextVideo = $this->NextVideo();
		if($nextVideo && $nextVideo->exists()){
			return $nextVideo->First()->Link();
		}else{
			return null;
		}
	}

	public function NextVideoTitle(){
		$nextVideo = $this->NextVideo();
		if($nextVideo && $nextVideo->exists()){
			return $nextVideo->First()->Title;
		}else{
			return null;
		}
	}

	public function PrevVideoLink(){
		$prevVideo = $this->PrevVideo();
		if ($prevVideo && $prevVideo->exists()) {
			return $prevVideo->First()->Link();
		} else {
			return null;
		}
	}
	
	public function PrevVideoTitle(){
		$prevVideo = $this->PrevVideo();
		if ($prevVideo && $prevVideo->exists()) {
			return $prevVideo->First()->Title;
		} else {
			return null;
		}
	}

	protected function onBeforeWrite(){
		parent::onBeforeWrite();
		if($this->URLSegment){
				$this->URLSegment = Surgery::generate_url($this->URLSegment);
			}else{
				$this->URLSegment = Surgery::generate_url($this->Title);
			}
		}
}