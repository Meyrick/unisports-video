<?php

class Surgery extends DataObject {

	static $db = array(
		'SurgeryName' => 'Varchar(255)',
		'BreadCrumbTitle' => 'Varchar(255)'
	);

	static $has_many = array(
		'VideoPhases' => 'VideoPhase'
	);

	static $summary_fields = array(
		"SurgeryName"
	);

	//if breadcrumbtitle is not null return it else return the Surgeryname
	public function BreadCrumb(){
		return $this->BreadCrumbTitle ? $this->BreadCrumbTitle : $this->SurgeryName;
	}

	static function generate_url($title){
		$filter = URLSegmentFilter::create();
		return $filter->filter($title);
	}

	public function getCMSFields(){
		$fields = parent::getCMSFields();

		$fields->removeByName('SortOrder');
		$fields->removeByName('VideoPhases');

		$config = $config = GridFieldConfig_RecordEditor::create();
		$gf = new GridField('VideoPhases', "Edit Video Stages", $this->VideoPhases());
		$gf->setConfig($config);

		if ($this->ID) {
			$config->addComponent(new GridFieldOrderableRows('SortOrder'));
		}
		$fields->addFieldToTab('Root.VideoPhases', $gf);


		return $fields;
	}

}

